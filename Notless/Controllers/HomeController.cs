﻿using Notless.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Notless.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            HttpCookie cookieU = Request.Cookies["UserName"];
            HttpCookie cookieR = Request.Cookies["Role"];

            if (cookieR != null && cookieU != null)
            {
                Session["UserName"] = cookieU.Value.ToString();
                Session["Role"] = cookieR.Value.ToString();
            }

            RedStoreContext db = new RedStoreContext();

            var newP = db.Products.OrderByDescending(p => p.ProductID).Take(5);

            foreach (var item in newP)
            {
                if (item.Description.Length > 120)
                {
                    item.Description = item.Description.Substring(0, 120) + "...";
                }
            }

            var features = db.Products.OrderBy(p => Guid.NewGuid()).Take(5);

            foreach (var item in features)
            {
                if (item.Description.Length > 120)
                {
                    item.Description = item.Description.Substring(0, 120) + "...";
                }
            }

            ViewBag.Features = features;

            var sales = db.Products.OrderBy(p => p.UnitPrice).Take(5);

            foreach (var item in sales)
            {
                if (item.Description.Length > 120)
                {
                    item.Description = item.Description.Substring(0, 120) + "...";
                }
            }

            ViewBag.Sales = sales;

            return View(newP);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Part5()
        {
            return View();
        }
    }
}