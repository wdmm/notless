﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Notless.Models;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Notless.Controllers
{
    public class ProductController : Controller
    {
        private RedStoreContext db = new RedStoreContext();

        // GET: Product
        public ActionResult Page()
        {

            var products = db.Products.Include(p => p.ProductType);
            ViewBag.listProducts = db.Products.ToList();

            return View(products.ToList());
        }

        public ActionResult Index(int page = 1, string cat = "all")
        {
            int nb_Itm_pg = 7;
            if (cat == "all")
            {
                ViewBag.Max = db.Products.Count() % nb_Itm_pg > 0 ? db.Products.Count() / nb_Itm_pg + 1 : db.Products.Count() / nb_Itm_pg;
            }
            else
            {
                ViewBag.Max = db.Products.Where(p => p.ProductType.Name == cat).Count() % nb_Itm_pg > 0 ? db.Products.Where(p => p.ProductType.Name == cat).Count() / nb_Itm_pg + 1 : db.Products.Where(p => p.ProductType.Name == cat).Count() / nb_Itm_pg;
            }
            ViewBag.Page = page;
            ViewBag.Cat = cat;

            //if ((Math.Abs((db.Products.Count() - (page * nb_Itm_pg))) > nb_Itm_pg && page != 1) || page <=0
            //    || (Math.Abs((db.Products.Where(p => p.ProductType.Name == cat).Count() - (page * nb_Itm_pg))) > nb_Itm_pg && page != 1))
            //{
            //    return null;
            //}
            //else
            //{
                if (cat == "all")
                {
                if ((page*nb_Itm_pg) <= db.Products.Count())
                    {
                        return View((from p in db.Products
                                 select p).OrderByDescending(p => p.ProductID).Skip((page-1) * nb_Itm_pg).Take(nb_Itm_pg));
                    }
                    else
                    {
                        return View((from p in db.Products
                                     select p).OrderByDescending(p => p.ProductID).Skip((page - 1) * nb_Itm_pg));
                    }
                }
                else
                {
                if ((page * nb_Itm_pg) <= db.Products.Where(p => p.ProductType.Name == cat).Count())
                    {
                        return View((from p in db.Products
                                     select p).OrderByDescending(p => p.ProductID).Where(p => p.ProductType.Name == cat).Skip((page - 1) * nb_Itm_pg).Take(nb_Itm_pg));
                    }
                    else
                    {
                        return View((from p in db.Products
                                     select p).OrderByDescending(p => p.ProductID).Where(p => p.ProductType.Name == cat).Skip((page - 1) * nb_Itm_pg));
                    }
                }
                
            //}

        }

        // GET: Product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            //ViewBag.ProductTypeID = new SelectList(db.ProductTypes, "ProductTypeId", "Name");

            List<SelectListItem> ProductTypeID = new List<SelectListItem>();

            foreach (var item in db.ProductTypes)
            {
                ProductTypeID.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.ProductTypeId.ToString()
                });
            }
            
            ViewBag.ProductTypeID = ProductTypeID;


            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,ProductName,ProductTypeID,StockLevel,UnitPrice,UnitMeasure,Description,Style")] Product product)
        {
            var file = Request.Files;
            if (file[1].ContentLength > 0)
            {
                var fileName = Path.GetFileName(file[1].FileName);
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                file[1].SaveAs(path);
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                product.Image = br.ReadBytes((int)fs.Length);
                fs.Close();
            }
            
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductTypeID = new SelectList(db.ProductTypes, "ProductTypeId", "Name", product.ProductTypeID);
            return View(product);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductTypeID = new SelectList(db.ProductTypes, "ProductTypeId", "Name", product.ProductTypeID);
            return View(product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,ProductName,ProductTypeID,StockLevel,UnitPrice,UnitMeasure,Description,Style,Image")] Product product, HttpPostedFileBase fileBase)
        {
            /*
            var jName = Request.Files[0] ?? null;
            
            if (jName != null)
            {
                MemoryStream target = new MemoryStream();
                jName.InputStream.CopyTo(target);
                var s = Convert.ToBase64String(target.ToArray());
                s = s.Replace('-', '+');
                s = s.Replace('_', '/');
                product.Image = Convert.FromBase64String(s);
            }
            */

            try
            {
                if (ModelState.IsValid)
                {

                    try
                    {
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = ex.Message;
                        return View();
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Message = "BZX";
                    ViewBag.ProductTypeID = new SelectList(db.ProductTypes, "ProductTypeId", "Name", product.ProductTypeID);
                    return View();
                }
            }
            catch (Exception xx)
            {
                ViewBag.Message = xx.Message;
                return View();
            }
            

            ViewBag.ProductTypeID = new SelectList(db.ProductTypes, "ProductTypeId", "Name", product.ProductTypeID);




            return View(product);
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
