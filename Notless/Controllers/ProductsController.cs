﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Notless.Models;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Notless.Controllers
{
    public class ProductsController : ApiController
    {
        private RedStoreContext db = new RedStoreContext();

        // GET: api/Products
        public IQueryable<ProductDTO> GetProducts()
        {
            var products = from p in db.Products
                           select new ProductDTO
                           {
                               ProductID = p.ProductID,
                               ProductName = p.ProductName,
                               ProductTypeID = p.ProductTypeID,
                               Image =  "http://" + HttpContext.Current.Request.Url.Authority + ("/UploadedFiles/image") + p.ProductID + ".jpg" //+ Path.GetExtension(Directory.GetFiles(HttpContext.Current.Server.MapPath("~/UploadedFiles/"), "image" + p.ProductID + "*")[0])
                           };
            /*
            foreach (var item in products)
            {
                item.Image = "data:image/jpg;base64," + Convert.ToBase64String(db.Products.Where(p => p.ProductID == item.ProductID).Select(p => p.Image).FirstOrDefault());
            }
            */

            return (products);
        }


        /*
        public IQueryable<ProductDTO> GetProducts(int page = 1)
        {
            int nb_Itm_pg = 6;
            int max_Pg = db.Products.Count() % nb_Itm_pg > 0 ? db.Products.Count() / nb_Itm_pg + 1 : db.Products.Count() / nb_Itm_pg;
            ResponseMessage(new HttpResponseMessage(HttpStatusCode.Accepted));

            var products = (from p in db.Products
                           select new ProductDTO
                           {
                               ProductID = p.ProductID,
                               ProductName = p.ProductName,
                               ProductTypeID = p.ProductTypeID,
                               Image = "http://" + HttpContext.Current.Request.Url.Authority + ("/UploadedFiles/image") + p.ProductID + ".jpg" //+ Path.GetExtension(Directory.GetFiles(HttpContext.Current.Server.MapPath("~/UploadedFiles/"), "image" + p.ProductID + "*")[0])
                           }).OrderByDescending(p => p.ProductID).Skip(page - 1 * nb_Itm_pg).Take(nb_Itm_pg);

            return (products);
        }
        */

        // GET: api/Products/5
        //[ResponseType(typeof(Product))]
        //[ResponseType(typeof(HttpContext))]
        public async Task<IHttpActionResult> GetProduct(int id)
        {
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            //Image from byte[]
            byte[] image = product.Image;
            using (MemoryStream ms = new MemoryStream(image))
            {
                ms.Write(image, 0, image.Length);
                Image i = Image.FromStream(ms, true);
                i.Save(HttpContext.Current.Server.MapPath("~/UploadedFiles/image") + id + ".jpg", ImageFormat.Jpeg);
            }

            ProductDetailsDTO productDetails = new ProductDetailsDTO
            {
                ProductID = product.ProductID,
                ProductName = product.ProductName,
                ProductTypeID = product.ProductTypeID,
                StockLevel = product.StockLevel,
                UnitPrice = product.UnitPrice,
                UnitMeasure = product.UnitMeasure,
                Description = product.Description,
                Style = product.Style,
                Image = "http://" + HttpContext.Current.Request.Url.Authority + ("/UploadedFiles/image") + id + ".jpg"
            };

            //return Ok(product);
            return Ok(productDetails);
        }

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.ProductID)
            {
                return BadRequest();
            }

            db.Entry(product).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        /*
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Products.Add(product);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = product.ProductID }, product);
        }
        */

        [Route("api/products/save")]
        [HttpPost]
        [ResponseType(typeof(Product))]
        public IHttpActionResult SaveProduct(Product product)
        {
            string jName = null;
            try
            {
                jName = Directory.GetFiles(HttpContext.Current.Server.MapPath("~/UploadedFiles/"), "image" + (db.Products.Max(p => p.ProductID) + 1) + "*")[0];
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ErrDB", "Err Img");
                return BadRequest(ModelState);
            }

            //File.WriteAllText(HttpContext.Current.Server.MapPath("~/UploadedFiles/") + "WriteText.txt", jName);
            /*
            File.Copy(Directory.GetFiles(HttpContext.Current.Server.MapPath("~/UploadedFiles/"), "BodyPart*")[0],
                        HttpContext.Current.Server.MapPath("~/UploadedFiles/") + "product.json");
            File.Delete(Directory.GetFiles(HttpContext.Current.Server.MapPath("~/UploadedFiles/"), "BodyPart*")[0]);
            string json;
            using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("~/UploadedFiles/") + "product.json"))
            {
                json = r.ReadToEnd();
            }
            */
            //Product product = JsonConvert.DeserializeObject<Product>(json);
            if(jName != null)
            {
                FileStream fs = new FileStream(jName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);

                product.Image = br.ReadBytes((int)fs.Length);
                fs.Close();
                br.Close();
            }

            if (product.ProductTypeID == 0)
                ModelState.AddModelError("product.ProductTypeID", "Select product type.");
            if (product.StockLevel == 0)
                ModelState.AddModelError("product.StockLevel", "Stock level is required.");
            if (product.UnitPrice == 0)
                ModelState.AddModelError("product.UnitPrice", "Enter unit price.");

            if (!ModelState.IsValid)
            {
                try
                {
                    //File.Delete(jName);
                }
                catch (Exception ex)
                {

                }
                return BadRequest(ModelState);
            }
            try
            {
                db.Products.Add(product);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return BadRequest(ModelState + ex.Message);
            }
            return Ok(product.ToString());//CreatedAtRoute("DefaultApi", new { id = product.ProductID }, product);
        }

        ///[HttpPost]
        public Task<HttpResponseMessage> Post()
        {
            List<string> savedFilePath = new List<string>();
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            string rootPath = HttpContext.Current.Server.MapPath("~/UploadedFiles");
            var provider = new MultipartFileStreamProvider(rootPath);

            //var file = HttpContext.Current.Request.Files;
            //File.WriteAllText(@"C:\Users\Vdm\Notless\Notless\UploadedFiles\WriteText.txt", file.Count.ToString());

            var task = Request.Content.ReadAsMultipartAsync(provider).
                ContinueWith<HttpResponseMessage>(t =>
                {
                    if (t.IsCanceled || t.IsFaulted)
                    {
                        Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                    }
                    foreach (MultipartFileData item in provider.FileData)
                    {
                        if(provider.FileData.Equals(null))
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No load image.");
                        try
                        {
                            string name = item.Headers.ContentDisposition.FileName.Replace("\"", "");
                            int maxID = 1;
                            if (db.Products.Count() > 0)
                            {
                                maxID = db.Products.Max(p => p.ProductID);
                            }
                            //if (Path.GetExtension(name) != ".jpg") return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Only .jpg image allowed.");

                            //File.WriteAllText(@"C:\Users\Vdm\Notless\Notless\UploadedFiles\WriteText.txt", HttpContext.Current.Server.MapPath("~/UploadedFiles/") + "image" + maxID + Path.GetExtension(name));
                            
                            string newFileName = /*Guid.NewGuid()*/"image" + (maxID + 1) + Path.GetExtension(name);

                            if (File.Exists(Path.Combine(rootPath, newFileName)))
                            {
                                try
                                {
                                    File.Delete(Path.Combine(rootPath, newFileName));
                                }
                                catch (Exception ex)
                                {
                                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "We are a problem. Please try again.");
                                }
                            }
                            else
                            {
                                File.Move(item.LocalFileName, Path.Combine(rootPath, newFileName));
                                
                                Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                                string fileRelativePath = "~/UploadedFiles/" + newFileName;
                                Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute(fileRelativePath));
                                savedFilePath.Add(fileFullPath.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            string message = ex.Message + "%LABEL%";
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
                        }
                    }
                    if (savedFilePath.Count == 0) return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Try again.");
                    return Request.CreateResponse(HttpStatusCode.Created, savedFilePath);
                });
            return task;
        }



        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> DeleteProduct(int id)
        {
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            await db.SaveChangesAsync();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.ProductID == id) > 0;
        }
    }
}