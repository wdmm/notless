﻿using Notless.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace Notless.Controllers
{
    public class ShoppingCartController : Controller
    {
        RedStoreContext storeDB = new RedStoreContext();
        private string CartID;
        List<Cart> list = new List<Cart>();
        
        // GET: ShoppingCart
        [HttpGet]
        public ActionResult Index()
        {
            //ViewBag.listProducts = storeDB.Products.ToList();
            //storeDB.Products.ToList()
            if (Session["UserID"] != null)
            {
                CartID = Session["UserID"].ToString();
                if (Session["CartID"] == null)
                {
                    Session["CartID"] = CartID;
                }
                foreach (var item in storeDB.Carts.Where(c => c.CartID == CartID).ToList())
                {
                    list.Add(item);
                }
            }
            else
            {
                if (Session["CartID"] == null)
                {
                    CartID = new Guid().ToString();
                    Session["CartID"] = CartID;
                }
                list = Session["listCart"]!= null? (List<Cart>)Session["listCart"] : list;
            }
            Session["listCart"] = list;
            return View(list);
        }

        
        public ActionResult Del(int id)
        {
            list = (List<Cart>)Session["listCart"];
            if(list != null)
            {
                list.RemoveAt(IndLst(id));
                if(Session["UserID"] != null)
                try
                {
                    var cart = Session["UserID"].ToString();
                    storeDB.Carts.Remove(storeDB.Carts.SingleOrDefault(c => c.ProductID == id && c.CartID == cart));
                    storeDB.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Error in db.";
                }
            }
            
            Session["listCart"] = list;

            return View("Index",list);
        }
        

        // GET: /Store/AddToCart/5
        [HttpGet]
        public ActionResult AddToCart(int id)
        {
            // Retrieve the product from the database
            var addProduct = storeDB.Products.Find(id);

            if (Session["UserID"] == null)
            {
                if (Session["CartID"] == null)
                {
                    CartID = new Guid().ToString();
                    Session["CartID"] = CartID;
                }
            }
            else
            {
                Session["CartID"] = Session["UserID"];
                CartID = Session["UserID"].ToString();
                if (IndLst(id) == -1)
                    storeDB.Carts.Add(new Cart { CartID = Session["CartID"].ToString(), Count = 1, DateCreated = DateTime.Now, Product = addProduct, ProductID = addProduct.ProductID });
                else
                {
                    var c = storeDB.Carts.SingleOrDefault(p => p.ProductID == id);
                    if (c != null) c.Count++;
                }
                storeDB.SaveChanges();
            }
            
            if (Session["listCart"] == null)
            {
                if (Session["UserID"] != null) Session["CartID"] = Session["UserID"];
                list.Add(new Cart { CartID = Session["CartID"].ToString(), Count = 1, DateCreated = DateTime.Now, Product = addProduct, ProductID = addProduct.ProductID });
            }
            else
            {
                list = (List<Cart>)Session["listCart"];

                if (IndLst(id) == -1)
                    list.Add(new Cart { CartID = Session["CartID"].ToString(), Count = 1, DateCreated = DateTime.Now, Product = addProduct, ProductID = addProduct.ProductID });
                else
                    list[IndLst(id)].Count++;
            }

            Session["listCart"] = list;

            return RedirectToAction("Index", list);
        }

        private int IndLst(int id)
        {
            
            //list = (List<Cart>)Session["listCart"];
            if(Session["listCart"] != null || list != null)
            foreach (var item in list)
            {
                if (item.ProductID == id)
                    return list.IndexOf(item);
            }
            return -1;
        }

        [HttpGet]
        public ActionResult Order()
        {
            if (Session["UserID"] != null)
            {
                var id = Convert.ToInt32(Session["UserID"]);
                    var ord = storeDB.Orders.Where(o => o.UserID == id).ToList();
                    if(ord != null)
                        return View(ord);
            }
            
            return RedirectToAction("Login", "User", "");
        }

        
        public ActionResult PlaceOrder(int? id)
        {
            if (Session["UserID"] != null && list != null)
            {
                var usr = Session["UserID"].ToString();
                list = storeDB.Carts.Where(c => c.CartID == usr).ToList();
                try
                {
                    foreach (var item in list)
                    {
                        storeDB.Orders.Add(new Order
                        {
                            OrderDate = DateTime.Now,
                            OrderQty = item.Count,
                            Product = item.Product,
                            ProductID = item.ProductID,
                            UnitPrice = item.Product.UnitPrice,
                            UserID = Convert.ToInt32(Session["UserID"]),
                            Total = item.Count * item.Product.UnitPrice
                        });

                            try
                            {
                                storeDB.Carts.Remove(storeDB.Carts.SingleOrDefault(c => c.ProductID == item.ProductID && c.CartID == usr));
                            }
                            catch (Exception ex)
                            {
                                ViewBag.Message = "Error in db.";
                            }


                        storeDB.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Error to acces db.";
                }

            }
            else
            {
                return RedirectToAction("Login", "User", "");
            }
            return RedirectToAction("Order");
        }


        public ActionResult Cancel(int id)
        {

            var usr = Convert.ToInt32(Session["UserID"]);

            try
            {
                var cart = usr.ToString();
                storeDB.Orders.Remove(storeDB.Orders.SingleOrDefault(c => c.OrderID == id && c.UserID == usr));
                storeDB.SaveChanges();
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Error in db.";
            }

            var ord = storeDB.Orders.Where(o => o.UserID == usr).ToList();
            if (ord == null)
                return RedirectToAction("Order");

            return RedirectToAction("Order", ord);
        }

        public ActionResult Update(int id, int count)
        {
            if(count > 0)
            {
                if((List<Cart>)Session["listCart"] != null)
                    list = (List<Cart>)Session["listCart"];
                foreach (var item in list)
                {
                    if (item.ProductID == id)
                    {
                        item.Count = count;
                        if(Session["UserID"] != null)
                        {
                            var usr = Session["UserID"].ToString();
                            storeDB.Carts.Where(c => c.ProductID == id && c.CartID == usr).FirstOrDefault().Count = count;
                            storeDB.SaveChanges();
                        }
                    
                    }
                }
            }
            
            Session["listCart"] = list;
            return RedirectToAction("Index", list);
        }

        //
        // AJAX: /ShoppingCart/RemoveFromCart/5
        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {
            // Remove the item from the cart
            //var cart = ShoppingCart.GetCart(this.HttpContext);
            
            // Get the name of the album to display confirmation
            string Name = storeDB.Carts
                .Single(item => item.RecordID == id).Product.ProductName;

            // Remove from cart
            //int itemCount = cart.RemoveFromCart(id);

            // Display the confirmation message
            /*
            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(Name) +
                    " has been removed from your shopping cart.",
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                DeleteId = id
            };
            */
            return Json("");
        }
        //
        // GET: /ShoppingCart/CartSummary
        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            //var cart = ShoppingCart.GetCart(this.HttpContext);

            //ViewData["CartCount"] = cart.GetCount();
            return PartialView("CartSummary");
        }
    }
}