﻿using Notless.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Session;
using Microsoft.AspNet.Http;
using System.Web.Security;
using System.Web.Configuration;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net;
using System.Collections.Specialized;
using System.IO;
using System.Web.Script.Serialization;
using System.Data.Entity.Validation;

namespace Notless.Controllers
{
    public class MyObject
    {
        public string success { get; set; }
        public DateTime challenge_ts { get; set; }
        public string hostname { get; set; }
        public string[] error_codes { get; set; }

    }

    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            if (Session["Role"].ToString() == "3")
                return View();
            else
                return RedirectToAction("Dashboard");
        }

        public ActionResult Dashboard()
        {
            HttpCookie cookieU = Request.Cookies["UserName"];
            HttpCookie cookieR = Request.Cookies["Role"];

            if (Session["UserID"] != null)
            {
                
                using (RedStoreContext db = new RedStoreContext())
                {
                    int s = Convert.ToInt32(Session["UserID"]);
                    var usr = db.Users.Where(u => u.UserID == s).FirstOrDefault();
                    return View(usr);
                }
                
                //return View();
            }
            else if (cookieU != null && cookieR != null)
            {
                var u = cookieU.Value.ToString();
                RedStoreContext context = new RedStoreContext();
                var usr = context.Users.Where(x => x.UserName == u).FirstOrDefault();
                Session["UserID"] = usr.UserID;
                Session["Role"] = usr.RoleID;
                return View(usr);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Register()
        {
            ViewBag.Country = new SelectList(new List<string>{
                "Afghanistan", "Albania",
                "Algeria",     "Andorra",
                "Angola",      "Antigua and Barbuda",
                "Argentina",   "Armenia",
                "Aruba",       "Australia",
                "Austria",     "Azerbaijan",
                "Bahamas, The","Bahrain",
                "Bangladesh",  "Barbados",
                "Belarus",     "Belgium",
                "Belize",      "Benin",
                "Bhutan",      "Bolivia",
                "Bosnia and Herzegovina",
                "Botswana",    "Brazil",
                "Brunei",      "Bulgaria",
                "Burkina Faso","Burma",
                "Burundi",     "Cambodia",
                "Cameroon",    "Canada",
                "Cabo Verde",  "Central African Republic",
                "Chad",        "Chile",
                "China",       "Colombia",
                "Comoros",     "Congo, Democratic Republic of the",
                "Congo, Republic of the",
                "Costa Rica",  "Cote d'Ivoire",
                "Croatia",     "Cuba",
                "Curacao",
                "Cyprus",
                "Czechia",
                "Denmark",
                "Djibouti",
                "Dominica",
                "Dominican Republic",
                "East Timor (see Timor-Leste)",
                "Ecuador",
                "Egypt",
                "El Salvador",
                "Equatorial Guinea",
                "Eritrea",
                "Estonia",
                "Ethiopia",
                "Fiji",
                "Finland",
                "France",
                "Gabon",
                "Gambia, The",
                "Georgia",
                "Germany",
                "Ghana",
                "Greece",
                "Grenada",
                "Guatemala",
                "Guinea",
                "Guinea-Bissau",
                "Guyana",
                "Haiti",
                "Holy See",
                "Honduras",
                "Hong Kong",
                "Hungary",
                "Iceland",
                "India",
                "Indonesia",
                "Iran",
                "Iraq",
                "Ireland",
                "Israel",
                "Italy",
                "Jamaica",
                "Japan",
                "Jordan",
                "Kazakhstan",
                "Kenya",
                "Kiribati",
                "Korea, North",
                "Korea, South",
                "Kosovo",
                "Kuwait",
                "Kyrgyzstan",
                "Laos",
                "Latvia",
                "Lebanon",
                "Lesotho",
                "Liberia",
                "Libya",
                "Liechtenstein",
                "Lithuania",
                "Luxembourg",
                "Macau",
                "Macedonia",
                "Madagascar",
                "Malawi",
                "Malaysia",
                "Maldives",
                "Mali",
                "Malta",
                "Marshall Islands",
                "Mauritania",
                "Mauritius",
                "Mexico",
                "Micronesia",
                "Moldova",
                "Monaco",
                "Mongolia",
                "Montenegro",
                "Morocco",
                "Mozambique",
                "Namibia",
                "Nauru",
                "Nepal",
                "Netherlands",
                "New Zealand",
                "Nicaragua",
                "Niger",
                "Nigeria",
                "North Korea",
                "Norway",
                "Oman",
                "Pakistan",
                "Palau",
                "Palestinian Territories",
                "Panama",
                "Papua New Guinea",
                "Paraguay",
                "Peru",
                "Philippines",
                "Poland",
                "Portugal",
                "Qatar",
                "Romania",
                "Russia",
                "Rwanda",
                "Saint Kitts and Nevis",
                "Saint Lucia",
                "Saint Vincent and the Grenadines",
                "Samoa",
                "San Marino",
                "Sao Tome and Principe",
                "Saudi Arabia",
                "Senegal",
                "Serbia",
                "Seychelles",
                "Sierra Leone",
                "Singapore",
                "Sint Maarten",
                "Slovakia",
                "Slovenia",
                "Solomon Islands",
                "Somalia",
                "South Africa",
                "South Korea",
                "South Sudan",
                "Spain",
                "Sri Lanka",
                "Sudan",
                "Suriname",
                "Swaziland",
                "Sweden",
                "Switzerland",
                "Syria",
                "Taiwan",
                "Tajikistan",
                "Tanzania",
                "Thailand",
                "Timor-Leste",
                "Togo",
                "Tonga",
                "Trinidad and Tobago",
                "Tunisia",
                "Turkey",
                "Turkmenistan",
                "Tuvalu",
                "Uganda",
                "Ukraine",
                "United Arab Emirates",
                "United Kingdom",
                "Uruguay",
                "Uzbekistan",
                "Vanuatu",
                "Venezuela",
                "Vietnam",
                "Yemen",
                "Zambia",
                "Zimbabwe"
            }, "Country");
            return View();
        }

        static string Sha256(string randomString)
        {
            SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString), 0, Encoding.UTF8.GetByteCount(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }


        public string CalculateMessageDigest(string originalMessage, HashAlgorithm hashAlgo)
        {
            return Convert.ToBase64String(hashAlgo.ComputeHash(Encoding.UTF8.GetBytes(originalMessage)));
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            user.RoleID = 1;

            if (user.Gender == "Gender")
            {
                ModelState.AddModelError("Gender", "Select gender.");
                return View();
            }

            if (ModelState.IsValid)
            {
                //ViewBag.Pass = Sha256(user.Password).ToString();
                user.Password = CalculateMessageDigest(user.Password, SHA512.Create());
                user.CreatedDate = DateTime.Now;
                
                using (RedStoreContext db = new RedStoreContext())
                {
                    if (db.Users.Where(u => u.UserName == user.UserName).FirstOrDefault() != null)
                    {
                        ModelState.AddModelError("UserName", "That username is already registered.");
                        return View();
                    }
                    else if (db.Users.Where(u => u.Email == user.Email).FirstOrDefault() != null)
                    {
                        ModelState.AddModelError("Email", "That email is already registered.");
                        return View();
                    }
                    else
                    {
                        user.LastName = user.LastName.Trim();
                        user.FirstName = user.FirstName.Trim();
                        db.Users.Add(user);
                        db.SaveChanges();
                    }
                    
                }

                if(user.UserID > 0)
                //Send activation email.
                SendActivationEmail(user);

                ModelState.Clear();
                ViewBag.Message = user.FirstName + " " + user.LastName + " successfully registered.";
            }
            else
            {
                ViewBag.Message = "Try again!";
            }

            return View();
        }


        public ActionResult Login()
        {
            return View();
        }

        public bool Verify(string remoteip, string response)
        {
            string secret = "6LfCvkgUAAAAAKnD-ri87gE8xDzs4qq1aPI6OCrF";
            bool Valid = false;
            //Request to Google Server
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create
            ("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + response + "&remoteip=" + remoteip);

            try
            {
                //Google recaptcha Response
                using (WebResponse wResponse = req.GetResponse())
                {

                    using (StreamReader readStream = new StreamReader(wResponse.GetResponseStream()))
                    {
                        string jsonResponse = readStream.ReadToEnd();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        MyObject data = js.Deserialize<MyObject>(jsonResponse);// Deserialize Json
                        Valid = Convert.ToBoolean(data.success);
                    }
                }
                
                return Valid;
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            //ViewBag.Message = Request["remember"];
            //return View();

            string remoteip = Request["userip"],
                   response = Request["g-recaptcha-response"];

            if (!Verify(remoteip, response))
            {
                ViewBag.Message = "Captcha is not valid.";
                return View();
            }        

            if (user.Email != null && user.Password != null)
            {
                using (RedStoreContext db = new RedStoreContext())
                {
                    user.Password = CalculateMessageDigest(user.Password, SHA512.Create());
                    var acc = db.Users.Where(u => (u.Email == user.Email && u.Password == user.Password) ||
                                                  (u.UserName == user.Email && u.Password == user.Password)).FirstOrDefault();
                    if(acc != null)
                    {
                        Session["UserID"] = acc.UserID.ToString();
                        Session["UserName"] = acc.UserName;
                        Session["Email"] = acc.Email;
                        Session["Role"] = acc.RoleID.ToString();

                        if(Request["remember"] == "on")
                        {
                            HttpCookie cookieU = new HttpCookie("UserName", acc.UserName.ToString())
                            {
                                Expires = DateTime.Now.AddDays(7)
                            };
                            HttpCookie cookieR = new HttpCookie("Role", acc.RoleID.ToString())
                            {
                                Expires = DateTime.Now.AddDays(7)
                            };

                            Response.Cookies.Add(cookieU); Response.Cookies.Add(cookieR);
                        }
                        
                        return RedirectToAction("Dashboard");
                    }
                    else
                    {
                        ModelState.AddModelError("","Username(Email) or Password is wrong.");
                        return View();
                    }
                }
            }
            else
            {
                ViewBag.Message = "Credentials can not be empty.";
            }

            return RedirectToAction("Login");
        }


        public ActionResult Retrieve()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Retrieve(User user)
        {
            Guid activationCode = Guid.NewGuid();
            RedStoreContext usersEntities = new RedStoreContext();

            var us = usersEntities.Users.Where(u => u.Email == user.Email).FirstOrDefault();

            usersEntities.UserActivations.Add(new UserActivation { UserID = us.UserID, ActivationCode = activationCode });

            usersEntities.SaveChanges();

            using (MailMessage mm = new MailMessage("red.store.sender@gmail.com", user.Email))
            {
                mm.Subject = "REDStore Account Modifier";
                string body = "Hello " + user.UserName + ",";
                body += "<br /><br />Please click the following link to change your password";
                body += "<br /><a href = '" + string.Format("{0}://{1}/User/Password/{2}?email={3}", Request.Url.Scheme, Request.Url.Authority, activationCode, user.Email) + "'>Click here to change your password.</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("red.store.sender@gmail.com", "ReDsToRe!@#");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
            ViewBag.Message = "Verify your email.";
            return View();
        }

        public ActionResult Password()
        {
            ViewBag.Message = "Invalid Verification code.";
            if (RouteData.Values["id"] != null)
            {
                Guid activationCode = new Guid(RouteData.Values["id"].ToString());
                RedStoreContext dbEntities = new RedStoreContext();
                var act = activationCode;
                UserActivation userActivation = dbEntities.UserActivations.Where(p => p.ActivationCode == act).FirstOrDefault();
                var usr = new User();
                if (userActivation != null)
                {
                    dbEntities.UserActivations.Remove(userActivation);
                    dbEntities.SaveChanges();
                    ViewBag.Message = "Control successful.  Enter your new password.";

                    //ViewBag.Email = RouteData.Values.ToString();

                    Uri myUri = new Uri(Request.Url.ToString());
                    string email = HttpUtility.ParseQueryString(myUri.Query).Get("email");

                    //ViewBag.Email = email;

                    if (email != null)
                    {
                        RedStoreContext db = new RedStoreContext();
                        usr = db.Users.Where(u => u.Email == email).FirstOrDefault();
                        db.SaveChanges();
                    }
                }
                if(usr != null)
                {
                    usr.Password = string.Empty;
                    return View(usr);
                }
                    
            }

            return View("Retrieve");
        }

        [HttpPost]
        public ActionResult Password(User user)
        {
            RedStoreContext db = new RedStoreContext();

            if(user.Email != null && user.Password != null)
            {
                //var email = Request["Email"].ToString();
                //var pass = Request["Password"].ToString();
                var x = db.Users.Where(u => u.Email == user.Email).FirstOrDefault();
                if (x != null)
                {
                    x.Password = CalculateMessageDigest(user.Password.Trim(), SHA512.Create());
                    
                    x.FirstName = x.FirstName.Trim();
                    x.LastName = x.LastName.Trim();
                }

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (DbEntityValidationResult validationError in ex.EntityValidationErrors)
                    {
                        Response.Write("Object: " + validationError.Entry.Entity.ToString());
                        Response.Write("");
                        foreach (DbValidationError err in validationError.ValidationErrors)
                        {
                            Response.Write(err.ErrorMessage + "");
                        }
                    }
                return View();
                }
            }

            return RedirectToAction("Login");
        }


        public ActionResult Logout()
        {
                Session.Clear();

            HttpCookie cookieU = new HttpCookie("UserName", string.Empty);
            HttpCookie cookieR = new HttpCookie("Role", string.Empty);
            cookieU.Expires = DateTime.Now.AddDays(-1);
            cookieR.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookieU); Response.Cookies.Add(cookieR);

            return RedirectToAction("Index", "Home");
        }


        public ActionResult Activation()
        {
            ViewBag.Message = "Invalid Activation code.";
            if (RouteData.Values["id"] != null)
            {
                Guid activationCode = new Guid(RouteData.Values["id"].ToString());
                RedStoreContext dbEntities = new RedStoreContext();
                var act = activationCode;
                UserActivation userActivation = dbEntities.UserActivations.Where(p => p.ActivationCode == act).FirstOrDefault();
                if (userActivation != null)
                {
                    try
                    {
                        dbEntities.UserActivations.Remove(userActivation);
                        dbEntities.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "Error in db.";
                    }

                    Uri myUri = new Uri(Request.Url.ToString());
                    string email = HttpUtility.ParseQueryString(myUri.Query).Get("email");


                    try
                    {
                        var usr = dbEntities.Users.Where(u => u.Email == email).FirstOrDefault();
                        usr.EmailActivated = true;
                        usr.FirstName = usr.FirstName.Trim();
                        usr.LastName = usr.LastName.Trim();

                        dbEntities.SaveChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        foreach (DbEntityValidationResult validationError in ex.EntityValidationErrors)
                        {
                            Response.Write("Object: " + validationError.Entry.Entity.ToString());
                            Response.Write("");
                            foreach (DbValidationError err in validationError.ValidationErrors)
                            {
                                Response.Write(err.ErrorMessage + "");
                            }
                        }
                        return View();
                    }
                    


                    ViewBag.Message = "Activation successful.";
                }
            }

            return View();
        }

        private void SendActivationEmail(User user)
        {
            Guid activationCode = Guid.NewGuid();
            RedStoreContext usersEntities = new RedStoreContext();
            
            usersEntities.UserActivations.Add(new UserActivation { UserID = user.UserID,  ActivationCode = activationCode });

            usersEntities.SaveChanges();

            using (MailMessage mm = new MailMessage("red.store.sender@gmail.com", user.Email))
            {
                mm.Subject = "REDStore Account Activation";
                string body = "Hello " + user.UserName + ",";
                body += "<br /><br />Please click the following link to activate your account";
                body += "<br /><a href = '" + string.Format("{0}://{1}/User/Activation/{2}?email={3}", Request.Url.Scheme, Request.Url.Authority, activationCode, user.Email) + "'>Click here to activate your account.</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("red.store.sender@gmail.com", "ReDsToRe!@#");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

    }
}