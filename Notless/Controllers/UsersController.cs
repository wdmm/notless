﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Notless.Models;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http.ModelBinding;

namespace Notless.Controllers
{
    public class UsersController : ApiController
    {
        private RedStoreContext db = new RedStoreContext();

        // GET: api/Users
        public IQueryable<UserDTO> GetUsers()
        {
            var users = from u in db.Users
                        select new UserDTO
                        {
                           UserID = u.UserID,
                           Email = u.Email,
                           FirstName = u.FirstName,
                           LastName = u.LastName
                        };
            return users;
            //return db.Users;
        }

        // GET: api/Users/5
        [ResponseType(typeof(UserDetailsDTO))]
        public async Task<IHttpActionResult> GetUser(int id)
        {
            UserDetailsDTO user = await db.Users.Select(b =>
                            new UserDetailsDTO()
                            {
                                UserID = b.UserID,
                                LastName = b.LastName,
                                FirstName = b.FirstName,
                                UserName = b.UserName,
                                Address = b.Address,
                                Gender = b.Gender,
                                Email = b.Email,
                                RoleID = b.RoleID
                            }).SingleOrDefaultAsync(b => b.UserID == id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/Users
        [ResponseType(typeof(UserDTO))]
        public async Task<IHttpActionResult> PostUser(User user)
        {
            if (user.Gender == "Gender")
                ModelState.AddModelError("user.Gender","Select gender from list.");
            if (db.Users.Where(u => u.UserName == user.UserName).FirstOrDefault() != null)
                ModelState.AddModelError("user.UserName","Username already exist.");
            if (db.Users.Where(e => e.Email == user.Email).FirstOrDefault() != null)
                ModelState.AddModelError("user.Email", "Email already exist.");

            user.RoleID = 1;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(user);
            await db.SaveChangesAsync();
            // return UserDTO
            var dto = new UserDTO()
            {
                UserID = user.UserID,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                RoleID = user.RoleID
            };

            return CreatedAtRoute("DefaultApi", new { id = user.UserID }, dto);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.UserID == id) > 0;
        }
    }
}