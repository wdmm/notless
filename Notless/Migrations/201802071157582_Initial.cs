namespace Notless.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        TrackingNumber = c.String(maxLength: 21, fixedLength: true),
                        OrderQty = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        UnitPrice = c.Decimal(nullable: false, storeType: "money"),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.Users", t => t.UserID)
                .Index(t => t.ProductID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        ProductName = c.String(nullable: false, maxLength: 55, unicode: false),
                        ProductTypeID = c.Int(nullable: false),
                        StockLevel = c.Int(nullable: false),
                        UnitPrice = c.Decimal(nullable: false, storeType: "money"),
                        UnitMeasure = c.String(nullable: false, maxLength: 5, fixedLength: true, unicode: false),
                        Style = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeID)
                .Index(t => t.ProductTypeID);
            
            CreateTable(
                "dbo.ProductType",
                c => new
                    {
                        ProductTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 55),
                        Description = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ProductTypeId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 21),
                        Password = c.String(nullable: false, maxLength: 34),
                        Email = c.String(nullable: false, maxLength: 55, unicode: false),
                        FirstName = c.String(nullable: false, maxLength: 21, fixedLength: true),
                        LastName = c.String(nullable: false, maxLength: 21, fixedLength: true),
                        Address = c.String(nullable: false, maxLength: 55),
                        Gender = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "UserID", "dbo.Users");
            DropForeignKey("dbo.Products", "ProductTypeID", "dbo.ProductType");
            DropForeignKey("dbo.Orders", "ProductID", "dbo.Products");
            DropIndex("dbo.Products", new[] { "ProductTypeID" });
            DropIndex("dbo.Orders", new[] { "UserID" });
            DropIndex("dbo.Orders", new[] { "ProductID" });
            DropTable("dbo.Users");
            DropTable("dbo.ProductType");
            DropTable("dbo.Products");
            DropTable("dbo.Orders");
        }
    }
}
