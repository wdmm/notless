namespace Notless.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_User_Model : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Phone", c => c.String(maxLength: 13));
            AlterColumn("dbo.Users", "Gender", c => c.String(maxLength: 1, fixedLength: true, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
            DropColumn("dbo.Users", "Phone");
        }
    }
}
