namespace Notless.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updating_User_Model_03 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false, maxLength: 6, fixedLength: true, unicode: false));
        }
    }
}
