namespace Notless.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updating_Model_Products_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Image");
        }
    }
}
