namespace Notless.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updating_Product_Model_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Products", "UnitMeasure", c => c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false));
            AlterColumn("dbo.Products", "Style", c => c.String(nullable: false, maxLength: 6, fixedLength: true, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Style", c => c.String(maxLength: 1, fixedLength: true, unicode: false));
            AlterColumn("dbo.Products", "UnitMeasure", c => c.String(nullable: false, maxLength: 5, fixedLength: true, unicode: false));
            DropColumn("dbo.Products", "Description");
        }
    }
}
