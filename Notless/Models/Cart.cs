﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class Cart
    {
        [Key]
        public int RecordID             { get; set; }
        public string CartID            { get; internal set; }
        public int ProductID            { get; set; }
        public int Count                { get; set; }
        public DateTime DateCreated     { get; set; }
        public virtual Product Product  { get; set; }
    }
}