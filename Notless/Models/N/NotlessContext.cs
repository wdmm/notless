﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class NotlessContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public NotlessContext() : base("name=NotlessContext")
        {
        }

        public System.Data.Entity.DbSet<Notless.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<Notless.Models.ProductType> ProductTypes { get; set; }

        public System.Data.Entity.DbSet<Notless.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<Notless.Models.Order> Orders { get; set; }
    }
}
