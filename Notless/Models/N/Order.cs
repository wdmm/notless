﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public string TrackingNumber { get; set; }
        public int OrderQty { get; set; }
        public int UnitPrice { get; set; }

        // Foreign Key
        public int ProductID { get; set; }
        // Navigation property
        public Product Product { get; set; }
        // Foreign Key
        public int UserID { get; set; }
        // Navigation property
        public User User { get; set; }
    }
}