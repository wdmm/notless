﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public int StockLevel { get; set; }
        [Required]
        public int UnitPrice { get; set; }
        [Required]
        public string UnitMeasure { get; set; }
        public char Style { get; set; }

        // Foreign Key
        public int ProductTypeID { get; set; }
        // Navigation property
        public ProductType ProductType { get; set; }
    }
}