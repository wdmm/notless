﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class ProductType
    {
        [Key]
        public int ProductTypeID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}