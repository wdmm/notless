namespace Notless.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Order
    {
        [Key]
        public int OrderID              { get; set; }

        [StringLength(35)]
        public string TrackingNumber    { get; set; }

        public int OrderQty             { get; set; }

        [Column(TypeName = "money")]
        public decimal UnitPrice        { get; set; }

        public DateTime OrderDate       { get; set; }

        public decimal Total            { get; set; }

        public int ProductID            { get; set; }

        public int UserID               { get; set; }

        public virtual Product Product  { get; set; }

        public virtual User User        { get; set; }
    }
}
