namespace Notless.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        //[ForeignKey("Cart")]
        public int ProductID                        { get; set; }

        [Required(ErrorMessage = "Product name is required.")]
        [StringLength(55, ErrorMessage = "Write a name of less than 55 characters.")]
        [RegularExpression(@"^[A-Z][a-zA-Z\s]+$", ErrorMessage = "Name start with one uppercase and must contain only uppercase, lowercase and spaces.")]
        public string ProductName                   { get; set; }

        [Required(ErrorMessage = "Product type is required.")]
        //[Range((0),int.MaxValue, ErrorMessage = "The ProductTypeID must be greater than 0.")]
        public int ProductTypeID                    { get; set; }

        [Required(ErrorMessage = "Indicate stock level.")]
        //[Range((0), int.MaxValue, ErrorMessage = "The price must be greater than 0.")]
        [RegularExpression(pattern: @"^[0-9]*$", ErrorMessage = "Only digits are accepted in stock level.")]
        public int StockLevel                       { get; set; }

        [Column(TypeName = "money")]
        [Required(ErrorMessage = "Enter unit price.")]
        //[Range(0.0, Double.MaxValue, ErrorMessage = "The price must be greater than 0.")]
        [RegularExpression(pattern: @"^[0-9]+([.,][0-9]{1,3})?$", ErrorMessage = "Only 5 decimal digits are accepted in unit price.")]
        public decimal UnitPrice                    { get; set; }

        [Required(ErrorMessage = "Select unit measure.")]
        [StringLength(8, ErrorMessage = "Write a name of unit measure of less than 8 characters.")]
        [RegularExpression(pattern: @"^[a-zA-Z0-9_.^-]*$", ErrorMessage = @"Just these characters: a-zA-Z0-9_.^- allowed in unit measure.")]
        public string UnitMeasure                   { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Describe the product a little.")]
        public string Description                   { get; set; }

        [StringLength(6)]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Select style of products.")]
        public string Style                         { get; set; }

        public byte[] Image                         { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders    { get; set; }
        public virtual ProductType ProductType      { get; set; }
        public virtual ICollection<Cart> Cart       { get; set; }
    }
}
