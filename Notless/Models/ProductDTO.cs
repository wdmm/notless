﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class ProductDTO
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int ProductTypeID { get; set; }
        public string Image { get; set; }
    }
}