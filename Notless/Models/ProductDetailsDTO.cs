﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class ProductDetailsDTO
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int ProductTypeID { get; set; }
        public int StockLevel { get; set; }
        public decimal UnitPrice { get; set; }
        public string UnitMeasure { get; set; }
        public string Description { get; set; }
        public string Style { get; set; }
        public string Image { get; set; }

    }
}