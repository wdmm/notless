namespace Notless.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        public int UserID                           { get; set; }

        [Display(Name = "Username")]
        [Required(ErrorMessage = "Username is required.")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed in username.")]
        [StringLength(21, ErrorMessage = "Not more than 21 characters in username.")]
        public string UserName                      { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [StringLength(513,ErrorMessage = "Not more than 513 characters in password.")]
        [RegularExpression(@"(?=^.\S{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", 
        ErrorMessage = "Password must contain at least 1 upper case letter, 1 lower case letter, 1 number or special character, 9 characters in length minimum.")]
        public string Password                      { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is required.")]
        [StringLength(55, ErrorMessage = "Not more than 55 characters in email.")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,5})+)$", ErrorMessage = "Only Alphabets, Hyphen, Dot, Asperand and Numbers allowed in email.")]
        public string Email                         { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets allowed in First Name.")]
        [StringLength(21, ErrorMessage = "Not more than 21 characters in First Name.")]
        public string FirstName                     { get; set; }

        [Required(ErrorMessage = "Lastname is required.")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets allowed in Last Name.")]
        [StringLength(21, ErrorMessage = "Not more than 21 characters in Last Name.")]
        public string LastName                      { get; set; }

        //[Required(ErrorMessage = "Phone is required.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers allowed in Phone.")]
        [StringLength(13, ErrorMessage = "Not more than 13 characters in Phone.")]
        public string Phone                         { get; set; }
        
        //[Required(ErrorMessage = "Address is required.")]
        [RegularExpression(@"^\s*\S+(?:\s+\S+){2}", ErrorMessage = "Address format is incorrect. Ex. str. Xxxxx 89")]
        [StringLength(55, ErrorMessage = "Not more than 55 characters in Address.")]
        public string Address                       { get; set; }

        [RegularExpression(@"^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$", ErrorMessage = "Enter a valid city name.")]
        [StringLength(34, ErrorMessage = "Not more than 34 characters in City field.")]
        public string City                          { get; set; }

        [RegularExpression(@"^([a-zA-Z]+|[a-zA-Z]+\s[a-zA-Z]+)*$", ErrorMessage = "Enter a valid state name.")]
        [StringLength(34, ErrorMessage = "Not more than 34 characters in State field.")]
        public string State                         { get; set; }

        [RegularExpression(@"^[A-Za-z-0-9\s]+[a-zA-Z0-9]$", ErrorMessage = "Enter a valid postal code.")]
        [StringLength(34, ErrorMessage = "Not more than 34 characters in Postal Code field.")]
        public string PostalCode                    { get; set; }

        [RegularExpression(@"^([a-zA-Z\s]+[a-zA-Z]{2,})*$", ErrorMessage = "Enter a valid country name.")]
        [StringLength(34, ErrorMessage = "Not more than 34 characters in Country field.")]
        public string Country                       { get; set; }

        [Required(ErrorMessage = "Gender is required.")]
        [StringLength(6, ErrorMessage = "Select gender from list.")]
        public string Gender                        { get; set; }

        public int RoleID                           { get; set; }

        public DateTime CreatedDate { get; set; }
        
        public DateTime? LastLoginDate { get; set; }

        public bool EmailActivated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders    { get; set; }
        public virtual UserRoles UserRoles          { get; set; }
    }
}
