﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class UserActivation
    {
        [Key]
        public int UserID { get; set; }
        public Guid ActivationCode { get; set; }
    }
}