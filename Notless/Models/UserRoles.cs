﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Notless.Models
{
    public class UserRoles
    {
        [Key]
        [ForeignKey("User")]
        public int RoleID { get; set; }
        [Required(ErrorMessage = "Role name is required.")]
        public string RoleName { get; set; }

        public string RoleDescription { get; set; }

        public int? Privilege { get; set; }

        public virtual User User { get; set; }
    }
}