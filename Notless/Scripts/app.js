﻿var ViewModel = function () {
    var self = this;
    self.users = ko.observableArray();
    self.error = ko.observable();
    self.detail = ko.observable();
    self.formAdd = ko.observable();
    //self.Gender = ko.observableArray(['Male', 'Female']);
    gender = ko.observableArray(['Gender', 'Male', 'Female']);
    shouldShowMessage = ko.observable(true);
    
    self.newUser = {
        UserName: ko.observable(),
        Email: ko.observable(),
        Password: ko.observable(),
        FirstName: ko.observable(),
        LastName: ko.observable(),
        Phone: ko.observable(),
        Address: ko.observable(),
        Gender: ko.observable()
    };
    
    var usersUri = '/api/users/';

    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null,
            success: function (result) {
                if (document.getElementById("errors") !== null)
                    document.getElementById("errors").style.display = "none";
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            //self.error(jqXHR.responseText);
            self.error(errorThrown);
        });
    }

    function getAllUsers() {
        ajaxHelper(usersUri, 'GET').done(function (data) {
            self.users(data);
        });
    }

    self.shouldShowMessageF = function () {
        $(document).ready(function () {
            $("#rec01").fadeOut(500);
        });
        setTimeout(function () { shouldShowMessage(false); }, 500);
    }

    self.getAddForm = function (item) {
        self.formAdd(item);
    }

    self.getUserDetail = function (item) {
        ajaxHelper(usersUri + item.UserID, 'GET').done(function (data) {
            self.detail(data);
            $("#rec01").fadeIn(500);
            setTimeout(function () { shouldShowMessage(true); }, 500);
        });
    };
    
    // Fetch the initial data.
    getAllUsers();

    self.addUser = function (formElement) {
        var user = {
            UserName: self.newUser.UserName(),
            Password: self.newUser.Password(),
            Email: self.newUser.Email(),
            FirstName: self.newUser.FirstName(),
            LastName: self.newUser.LastName(),
            Phone: self.newUser.Phone(),
            Address: self.newUser.Address(),
            Gender: self.newUser.Gender()
        };

        ajaxHelper(usersUri, 'POST', user).done(function (item) {
            self.users.push(item);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (document.getElementById("errors") !== null)
            {
                while (document.getElementById("errors").firstChild)
                {
                    document.getElementById("errors").removeChild(document.getElementById("errors").firstChild);
                }
                var errObj = JSON.parse(jqXHR.responseText),
                    element = document.getElementById("errors");
                if (errObj.ModelState["user.UserName"])
                {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.UserName"])));
                    element.appendChild(document.createElement("br"));
                    document.getElementById("inputUserName").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputUserName").style.border = "1px solid #AED581";
                }
                if (errObj.ModelState["user.Password"]) {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.Password"])));
                    element.appendChild(document.createElement("br"));
                    document.getElementById("inputPassword").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputPassword").style.border = "1px solid #AED581";
                }
                if (errObj.ModelState["user.Email"]) {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.Email"])));
                    element.appendChild(document.createElement("br"));
                    document.getElementById("inputEmail").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputEmail").style.border = "1px solid #AED581";
                }
                if (errObj.ModelState["user.FirstName"]) {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.FirstName"])));
                    element.appendChild(document.createElement("br"));
                    document.getElementById("inputFirstName").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputFirstName").style.border = "1px solid #AED581";
                }
                if (errObj.ModelState["user.LastName"]) {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.LastName"])));
                    element.appendChild(document.createElement("br"));
                    document.getElementById("inputLastName").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputLastName").style.border = "1px solid #AED581";
                }
                if (errObj.ModelState["user.Phone"]) {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.Phone"])));
                    element.appendChild(document.createElement("br"));
                    document.getElementById("inputPhone").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputPhone").style.border = "1px solid #AED581";
                }
                if (errObj.ModelState["user.Address"]) {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.Address"])));
                    element.appendChild(document.createElement("br"));
                    document.getElementById("inputAddress").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputAddress").style.border = "1px solid #AED581";
                }
                if (errObj.ModelState["user.Gender"]) {
                    element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["user.Gender"])));
                    document.getElementById("inputGender").style.border = "1px solid #e57373";
                }
                else {
                    document.getElementById("inputGender").style.border = "1px solid #AED581";
                }
                
                document.getElementById("errors").style.display = "block";
            }
        });
    };
};

ko.applyBindings(new ViewModel());