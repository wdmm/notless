﻿/*
function Type(name, id) {
        this.typeName = name;
        this.id = id;
    };
*/

var ViewModel = function () {
    var self = this;
    self.products = ko.observableArray();
    self.error = ko.observable();
    self.detail = ko.observable();
    self.formAdd = ko.observable();

    availableStyle = ko.observableArray(['Male', 'Female', 'Unisex']);
    availableUnitMeasure = ko.observableArray(['meter', 'units','m2']);

    

    /*
    availableType = ko.observableArray([
        new Type("Clothing", 1),
        new Type("Shoes", 2),
        new Type("Accessories", 3),
        new Type("Activewear", 5),
        new Type("Face + Body", 6)
    ]);

    ProductTypeID = ko.observable();
    */

    self.newProduct = {
        ProductName: ko.observable(),
        ProductTypeID: ko.observable(),
        StockLevel: ko.observable(),
        UnitPrice: ko.observable(),
        UnitMeasure: ko.observable(),
        Style: ko.observable(),
        Description: ko.observable(),
        Image: ko.observableArray()
    };

    var productsUri = '/api/products/';

    function getAllProducts() {
        self.error(''); // Clear error message
        return $.ajax({
            type: 'GET',
            url: productsUri,
            dataType: 'json',
            contentType: 'application/json',
            data: null,
            success: function (result) {
                if (document.getElementById("errors") !== null)
                    document.getElementById("errors").style.display = "none";
            }
        }).done(function (data) {
            self.products(data);
            //self.error(data);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            //self.error(jqXHR.responseText);
            self.error(errorThrown);
        });
    }

    getAllProducts();

    self.getProductDetail = function (item) {
            $.ajax({
                type: "GET",
                url: productsUri + item.ProductID,
                dataType: 'json',
                contentType: 'application/json'
            }).fail(function (jqXHR, textStatus, errorThrown) {
                self.error(jqXHR.responseText);
                self.error(errorThrown);
            }).done(function (data) {
                    self.detail(data);
            });


    };


    var sucImg = false,
        sucPr = false;

    self.addProduct = function (formElement) {
        self.error('');
        var filet = $("#btnUp").val(),
            allowedExtensions = /(\.jpg|\.jpeg)$/i;
        if (filet === '')
        {
            var element = document.getElementById("errors");
            element.appendChild(document.createElement("p").appendChild(document.createTextNode('Please select image of product.')));
            document.getElementById("errors").style.display = "block";
            //self.error('Please select image of product.');
        }
        else
        if (!allowedExtensions.exec(filet))
        {
            alert('Please upload file having extensions .jpeg/.jpg only.');
            filet.value = '';
            return false;
        }
        else
        {
            var file = $('#btnUp')[0],
                formData = new FormData();

            
            //Image preview
            if (document.getElementById('btnUp').files && document.getElementById('btnUp').files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '" class="col-sm-12 img-thumbnail"/>';
                };
                reader.readAsDataURL(document.getElementById('btnUp').files[0]);
            }
            

            formData.append('file', file.files[0]);

            var product = {
                ProductName: self.newProduct.ProductName(),
                ProductTypeID: self.newProduct.ProductTypeID(),
                StockLevel: self.newProduct.StockLevel(),
                UnitPrice: self.newProduct.UnitPrice(),
                UnitMeasure: self.newProduct.UnitMeasure(),
                Style: self.newProduct.Style(),
                Description: self.newProduct.Description()
            };
            //formData.append('product', JSON.stringify(product));
            if (sucImg === false)
            {
                $.ajax({
                url: '/api/products/',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (results) {
                    self.error('');
                    //for (i = 0; i < results.length; i++) {alert(results[i]);}
                    sucImg = true;
                    document.getElementById("errors").style.display = "none";
                    document.getElementById("fileInput").style.border = "2px solid #AED581";
                    document.getElementById("btnBtn").className = "btn btn-primary disabled";
                    document.getElementById("btnUp").readOnly = true;
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (document.getElementById("errors") !== null) {
                    while (document.getElementById("errors").firstChild) {
                        document.getElementById("errors").removeChild(document.getElementById("errors").firstChild);
                    }

                    var errObj = JSON.parse(jqXHR.responseText),
                        element = document.getElementById("errors");

                    if (errObj["Message"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj["Message"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("fileInput").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("fileInput").style.border = "1px solid #AED581";
                    }

                    document.getElementById("errors").style.display = "block";
                }
                self.error(jqXHR.responseText);
            });
            }

            if(sucImg === true)
            $.ajax({
                url: '/api/products/save',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: product ? JSON.stringify(product) : null,
                success: function (results) {
                    //alert(results);
                    alert("Succes.");
                    self.error('');
                    sucPr = true;
                    document.getElementById("errors").style.display = "none";
                    var nodes = document.getElementById('errors').childNodes;
                    for (var i = 0; i < nodes.length; i++) {
                        if (nodes[i].nodeName.toLowerCase() === 'p') {
                            nodes[i].style.border = "1px solid #AED581";
                        }
                    }
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (document.getElementById("errors") !== null) {
                    while (document.getElementById("errors").firstChild) {
                        document.getElementById("errors").removeChild(document.getElementById("errors").firstChild);
                    }

                    var errObj = JSON.parse(jqXHR.responseText),
                        element = document.getElementById("errors");

                    if (errObj.ModelState["product.ProductName"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["product.ProductName"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("inputProductName").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("inputProductName").style.border = "1px solid #AED581";
                    }
                    if (errObj.ModelState["product.ProductTypeID"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["product.ProductTypeID"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("inputProductType").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("inputProductType").style.border = "1px solid #AED581";
                    }
                    if (errObj.ModelState["product.StockLevel"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["product.StockLevel"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("inputStockLevel").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("inputStockLevel").style.border = "1px solid #AED581";
                    }
                    if (errObj.ModelState["product.UnitPrice"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["product.UnitPrice"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("inputUnitPrice").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("inputUnitPrice").style.border = "1px solid #AED581";
                    }
                    if (errObj.ModelState["product.UnitMeasure"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["product.UnitMeasure"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("inputUnitMeasure").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("inputUnitMeasure").style.border = "1px solid #AED581";
                    }
                    if (errObj.ModelState["product.Style"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["product.Style"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("inputStyle").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("inputStyle").style.border = "1px solid #AED581";
                    }
                    if (errObj.ModelState["product.Description"]) {
                        element.appendChild(document.createElement("p").appendChild(document.createTextNode(errObj.ModelState["product.Description"])));
                        element.appendChild(document.createElement("br"));
                        document.getElementById("inputDescription").style.border = "1px solid #e57373";
                    }
                    else {
                        document.getElementById("inputDescription").style.border = "1px solid #AED581";
                    }

                    document.getElementById("errors").style.display = "block";
                }


                self.error(jqXHR.responseText);
            });
            


        }
        
    };


};

$(function () {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready(function () {
        $(':file').on('fileselect', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (log.length > 10) {
                log = log.substring(0, 5) + '...' + log.substring(log.length, log.length-6);
            }

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }

        });
    });

});

ko.applyBindings(new ViewModel());

