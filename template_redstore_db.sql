IF (NOT EXISTS (SELECT name 
FROM master.dbo.sysdatabases 
WHERE ('[' + name + ']' = N'RedStore'
OR name = N'RedStore')))
CREATE DATABASE [RedStore]
GO

USE [RedStore]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Users table
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Users'))
BEGIN
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](21) NOT NULL DEFAULT (N'(user)' + cast(RAND()*13 as nvarchar(13))),
	[Password] [nvarchar](34) NOT NULL,
	[Email] [varchar](55) NOT NULL,
	[FirstName] [nchar](21) NOT NULL,
	[LastName] [nchar](21) NOT NULL,
	[Address] [nvarchar](55) NOT NULL,
	[Gender] [char](1) NOT NULL,
	PRIMARY KEY (UserID)
) ON [PRIMARY]
END
GO

--ProductType table
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'ProductType'))
BEGIN
CREATE TABLE [dbo].[ProductType](
	[ProductTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](55) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	PRIMARY KEY (ProductTypeId)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

--Products table
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Products'))
BEGIN
CREATE TABLE [dbo].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductTypeID] [int] NOT NULL,
	[StockLevel] [int] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[UnitMeasure] [char](5) NOT NULL,
	[Style] [char](1) NULL,
	PRIMARY KEY (ProductID),
    CONSTRAINT FK_ProductType FOREIGN KEY (ProductTypeID)
    REFERENCES ProductType(ProductTypeId)
) ON [PRIMARY]
END
GO

--Orders table
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Orders'))
BEGIN
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[TrackingNumber] [nchar](21) NULL,
	[OrderQty] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[UserID] [int] NOT NULL,
	PRIMARY KEY (OrderID),
	CONSTRAINT FK_Product FOREIGN KEY (ProductID)
    REFERENCES Products(ProductID),
	CONSTRAINT FK_User FOREIGN KEY (UserID)
    REFERENCES Users(UserID)
) ON [PRIMARY]
END
GO

